<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformComputedTwig;
use Drupal\webform\Plugin\WebformElementDisplayOnInterface;

/**
 * @WebformElement(
 *   id = "count_if_twig_not_empty",
 *   label = @Translation("Submission counter with condition"),
 *   description = @Translation("Provides an element that shows a counter
 *   across all submissions of the webform it sits on. Submissions are only
 *   counted if the Twig condition returns truthy output. Empty strings and 0
 *   are considered falsy."),
 *   category = @Translation("Computed Elements"),
 * )
 */
class CountIfTwigNotEmpty extends WebformComputedTwig {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
        'display_on' => WebformElementDisplayOnInterface::DISPLAY_ON_FORM,
        'counter_template' => '%d',
        'counter_template_format' => '',
        'debugging_output_per_submission' => FALSE,
      ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultBaseProperties() {
    $properties = parent::defineDefaultBaseProperties();
    unset($properties['prepopulate']);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['computed']['template']['#title'] = $this->t(
        'Condition'
      ).': '.$this->t('count if');
    $elements = array_keys($this->getWebform()->getElementsInitialized());
    $elements[] = 'key1';
    $elements[] = 'key2';
    $description = $this->t(
      'Submissions are only counted if the Twig condition returns truthy output. Empty strings and 0 are considered falsy.'
    );
    $description .= '<br><br>';
    $description .= $this->t('Examples are:').'<br>';
    $description .= sprintf('{{ data.%s }}', reset($elements)).'<br>';
    $description .= sprintf(
        '{%% if data.%s %%}is filled{%% endif %%}',
        next($elements)
      ).'<br>';
    $form['computed']['template']['#description'] = $description;
    $form['computed']['template']['#required'] = TRUE;

    // Inherited from base class, but not needed:
    unset($form['computed']['store'], $form['computed']['ajax'], $form['computed']['mode'], $form['computed']['whitespace'], $form['computed']['hide_empty']);

    $form['computed']['debugging_output_per_submission'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#title' => $this->t('Debugging output per submission'),
      '#description' => $this->t(
        'If checked, a table will be shown with the computed value for each submission.'
      ),
    ];
    $form['computed']['counter_template'] = ['#required' => TRUE] +
      $this->buildTemplate($form_state);

    return $form;
  }

  public function isInput(array $element) {
    // no need to export this, because the value is not per submission, but per form.
    return FALSE;
  }

  public function isContainer(array $element) {
    return FALSE;
  }

  /**
   * When saving the UI form (right-side bar), split the array into 2
   * properties. Having an array property confuses
   *
   * @param array $properties
   *   An associative array of submitted properties.
   * @param string $property_name
   *   The property's name.
   * @param mixed $property_value
   *   The property's value.
   * @param array $element
   *   The element whose properties are being updated.
   */
  protected function getConfigurationFormProperty(
    array &$properties,
    $property_name,
    $property_value,
    array $element
  ) {
    if ($property_name === 'counter_template') {
      // Right-hand side comes from Form API’s TextFormat class.
      // Left-hand side is the Webform element.
      $properties['counter_template'] = $property_value['value'];
      $properties['counter_template_format'] = $property_value['format'];
    }
    else {
      parent::getConfigurationFormProperty(
        $properties,
        $property_name,
        $property_value,
        $element
      );
    }
  }

  protected function buildTemplate($form_state): array {
    $element = $form_state->get('element_properties');

    $formElement = [
      '#type' => 'text_format',
      '#title' => $this->t(
        'Counter text template: the %d is replaced by the actual number'
      ),
      '#rows' => 2,
      '#default_value' => '%d',
    ];

    if (!empty($element['counter_template_format'])) {
      $formElement['#format'] = $element['counter_template_format'];
    }

    return $formElement;
  }

}
