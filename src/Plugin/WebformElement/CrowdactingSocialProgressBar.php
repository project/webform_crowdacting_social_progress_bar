<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_crowdacting_social_progress_bar\Element\CrowdactingSocialProgressBarElement;

/**
 * @WebformElement(
 *   id = "crowdacting_social_progress_bar",
 *   label = @Translation("Crowdacting social progress bar"),
 *   description = @Translation("Provides an element that shows a progress bar
 *   and/or a counter. The bar itself has no input, but you can add any element
 *   from the options family (radios, select). The keys for the options are
 *   parsed for numbers. Each number becomes a level in the bar. As soon as
 *   there are enough people in a level, it will be counted and displayed in a
 *   darker green than unreached levels. You can use a custom theme to change
 *   colors and styling. Unreached levels fill from the right. If you hover
 *   over an unreached level, it will show how many answers are still needed to
 *   reach the level."),
 *   category = @Translation("Computed Elements"),
 * )
 */
class CrowdactingSocialProgressBar extends CountIfTwigNotEmpty {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
        'source_element' => '',
        'show_limit_ratios' => FALSE,
        'show_overspills' => FALSE,
        'show_bar' => TRUE,
        'show_counter' => FALSE,
        'exponent' => 0.7,
        'show_intermediate_labels' => TRUE,
      ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['computed']['template']['#required'] = FALSE;
    $form['computed']['template']['#description'] .= '<br>'.t(
        'Leave empty to consider all submissions.'
      );
    unset($form['computed']['debugging_output_per_submission'], $form['computed']['counter_template']);

    $form['progress_bar'] = [
      '#weight' => -3000,
      '#type' => 'fieldset',
      '#title' => $this->t('Progress bar settings'),
    ];

    $options = [];
    $elements = $this->getWebform()->getElementsDecodedAndFlattened();
    foreach ($elements as $machine_name => $element) {
      if (in_array($element['#type'], ['radios', 'select'])) {
        $keys = CrowdactingSocialProgressBarElement::getLimitNumbersFromElement(
          $element
        );
        if ($keys) {
          $options[$machine_name] = $element['#title'].': '.join(', ', $keys);
        }
      }
    }
    $form['progress_bar']['source_element'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $options,
      '#title' => $this->t('Element results to show'),
      '#description' => $this->t(
        'Data source for the bar. The keys for the options are parsed for numbers. Each number becomes a sub-section in the bar.'
      ),

    ];
    $form['progress_bar']['show_limit_ratios'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#title' => $this->t('Show limit ratios as bullet list'),
    ];
    $form['progress_bar']['show_overspills'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#title' => $this->t('Show overspills in bullet list'),
      '#states' => [
        'visible' => [
          ':input[name="properties[show_limit_ratios]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['progress_bar']['show_bar'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#default_value' => TRUE,
      '#title' => $this->t('Show progress bar'),
      '#states' => [
        'required' => [
          ':input[name="properties[show_counter]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['progress_bar']['show_intermediate_labels'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#default_value' => TRUE,
      '#title' => $this->t('Show intermediate labels'),
      '#states' => [
        'visible' => [
          ':input[name="properties[show_bar]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['progress_bar']['exponent'] = [
      '#type' => 'number',
      '#default_value' => 0.7,
      '#min' => 0.05,
      '#max' => 1,
      '#step' => 0.05,
      '#title' => $this->t('Exponent for visually squeezing the progress bar'),
      '#description' => $this->t(
        'Use values like 0.2 for strong squeezing, 0.7 for moderate squeezing, and 1 for no squeezing.'
      ),
      '#states' => [
        'visible' => [
          ':input[name="properties[show_bar]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['progress_bar']['show_counter'] = [
      '#type' => 'checkbox',
      '#return_value' => TRUE,
      '#title' => $this->t(
        'Show counter for people that will participate in the action'
      ),
      '#description' => $this->t(
        'The counter will ignore answers when their limit is not yet reached.'
      ),
    ];
    $form['computed']['counter_template'] =
      [
        '#states' => [
          'visible' => [
            ':input[name="properties[show_counter]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="properties[show_counter]"]' => ['checked' => TRUE],
          ],
        ],
      ] +
      $this->buildTemplate($form_state);

    return $form;
  }

}
