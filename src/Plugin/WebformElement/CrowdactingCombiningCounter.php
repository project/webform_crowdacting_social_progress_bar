<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;

/**
 * @WebformElement(
 *   id = "crowdacting_combining_counter",
 *   label = @Translation("Crowdacting combining counter"),
 *   description = @Translation("Combines two or more progress bars into a
 *   single progress bar and/or counter. If a single submission commits to take
 *   part in several actions, it is only counted once, as soon as a single
 *   level is reached."),
 *   category = @Translation("Computed Elements"),
 * )
 */
class CrowdactingCombiningCounter extends CrowdactingSocialProgressBar {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
        'source_element' => '',
        'show_bar' => TRUE,
        'show_counter' => FALSE,
        'exponent' => 0.7,
        'counter_template' => '%d+',
        'counter_template_format' => '',
        'show_intermediate_labels' => TRUE,
      ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $options = [];
    $elements = $this->getWebform()->getElementsDecodedAndFlattened();
    foreach ($elements as $machine_name => $element) {
      if ($element['#type'] == 'crowdacting_social_progress_bar') {
        $options[$machine_name] = $element['#title'];
      }
    }
    $form['progress_bar']['source_element'] = [
        '#title' => $this->t('Progress bars to combine'),
        '#multiple' => TRUE,
        '#options' => $options,
        '#description' => $this->t(
          'Selecting one or more values is supported. To select multiple items, hold down CTRL (PC) or &#8984; (Mac) while selecting fields.'
        ),
      ] + $form['progress_bar']['source_element'];

    unset($form['progress_bar']['show_limit_ratios'], $form['progress_bar']['show_overspills']);

    return $form;
  }

}
