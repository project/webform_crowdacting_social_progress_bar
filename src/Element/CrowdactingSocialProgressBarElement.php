<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Element;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Twig\WebformTwigExtension;
use Drupal\webform\WebformSubmissionForm;

/**
 * This is a render element displaying a bar within a webform.
 *
 * @FormElement("crowdacting_social_progress_bar")
 */
class CrowdactingSocialProgressBarElement extends CountIfTwigNotEmptyElement {

  use CounterElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
        '#process' => [
          /** @uses processWebformComputed */
          [$class, 'processWebformComputed'],
        ],
        '#input' => FALSE,
        '#webform_submission' => NULL,
        '#theme_wrappers' => ['form_element'],
      ] + parent::getInfo();
  }

  /**
   * @param array $element
   * @param array $bins
   * @param array $counts
   * @param array $levelReached
   * @param int $committedCount
   */
  protected static function addBar(
    array &$element,
    array $bins,
    array $counts,
    array $levelReached,
    int $committedCount
  ): void {
    $element['#attributes']['class'][] = 'crowdacting-social-progress-bar';

    // Code for calculating screen position:
    $exponent = $element['#exponent'] ?? 0.7;
    if (!is_numeric($exponent) || !$exponent) {
      $exponent = 0.7;
    }
    /**
     * @param int $limit Answer count to calculate the percentage for
     *
     * @return float Percentage, between 0 (left) and 100 (right).
     */
    $getColFromLimit = fn(int $limit) => min(
      100,
      (pow($limit + 1, $exponent) - 1) /
      (pow(end($bins), $exponent) - 1)
      * 100
    );

    // Number labels above the bar
    $labels = "\n";
    foreach ($bins as $bin => $number) {
      $class = 'progress-label';
      if ($bin == 0) {
        $class .= ' label-first';
      }
      elseif (count($bins) - 1 == $bin) {
        $class .= ' label-last';
      }
      else {
        if (isset($element['#show_intermediate_labels']) && !$element['#show_intermediate_labels']) {
          continue;
        }
        $class .= ' label-'.strlen($number).'-chars';
        $class .= ' label-middle';
      }
      $col = $getColFromLimit($number);
      $labels .= "<div class='$class' style='left: $col%'><span>$number</span></div>\n";
    }

    // The filled colored boxes have two types: reached and potential
    $bars = "\n";
    $prevBinEnd = 0;
    $potentialCount = 0;
    foreach ($bins as $bin => $number) {
      $count = $counts[$number];
      if ($count == 0) {
        continue;
      }
      if (!$levelReached[$bin]) {
        $class = 'level';
        $potentialCount += $count;
        $title = Drupal::translation()->formatPlural(
            $count,
            'This bar contains 1 person.',
            'This bar contains @count people.',
          ).' '.Drupal::translation()->formatPlural(
            $number - $committedCount - $potentialCount,
            'They will join if one more person joins.',
            'They will join if @count others join.'
          );
        $left = $getColFromLimit($number);
        $right = 100 - $left;
        $width = $left - $getColFromLimit($number - $count);
        $potentialOverflown = $counts[$number] == $number - $prevBinEnd;
        $class .= $potentialOverflown ? ' level-potential-overflown' : ' level-potential';
        $bars .= "<div class='$class' style='right: $right%; width: $width%' data-number='$number' data-count='$count' title='$title'></div>\n";
      }
      $prevBinEnd = $number;
    }
    if ($committedCount) {
      $width = $getColFromLimit($committedCount);
      $title = Drupal::translation()->formatPlural(
        $committedCount,
        '1 person takes part.',
        '@count people take part.'
      );
      $class = in_array(
        $committedCount,
        $bins
      ) ? 'level level-reached' : 'level level-reached-overflown';
      $bars .= "<div class='$class' style='left: 0; width: $width%' data-count='$committedCount' title='$title'></div>\n";
    }

    // The unfilled bar section boxes
    foreach ($bins as $bin => $number) {
      if ($bin == 0) {
        continue;
      }
      $left = $getColFromLimit($bins[$bin - 1]);
      $width = $getColFromLimit($number) - $left;
      $bars .= "<div class='bar-section' style='left: $left%; width: $width%' data-number='$number'></div>\n";
    }

    if (!isset($element['#show_bar']) || $element['#show_bar']) {
      $element['#value'] .= '<progress-bar-row>'.$labels.$bars.'</tr></progress-bar-row>';
      $element['#attached']['library'][] = 'webform_crowdacting_social_progress_bar/style';
    }
    if (!empty($element['#show_counter'])) {
      self::addCounter($element, $committedCount);
    }
  }

  /**
   * Find webform entity and submission this element is embedded in.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $element
   *
   * @return array
   */
  protected static function getSubmission(
    FormStateInterface $form_state,
    array &$element,
  ): array {
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof WebformSubmissionForm) {
      /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
      $webform_submission = $form_object->getEntity();
      $webform_id = $webform_submission->getWebform()->get('id');

      // Store in cache until a submission is added/deleted/modified or the form
      // was changed using the Build tab.
      $element['#cache']['tags'] = [
        'webform_submission_list',
        'config:webform.webform.'.$webform_id,
      ];
      $element['#cache']['keys'] = ['progress-bar-'.$webform_id.'-'.$element['#admin_title']];

      return [$webform_submission, $webform_id];
    }
    else {
      $element['#value'] = 'ERROR: only submission forms are supported.';
    }

    return [NULL, NULL];
  }

  /**
   * Analyzes the webform options element: keys from the allowed answers are
   * returned as integers, even if the keys contain letters.
   *
   * @param array $element as return by Webform::getElementDecoded()
   *
   * @return int[]
   */
  public static function getLimitNumbersFromElement(array $element): array {
    if (empty($element['#options']) || !is_array($element['#options'])) {
      return [];
    }

    $keys = [0];
    foreach (array_keys($element['#options']) as $key) {
      $number = (int) preg_replace('/[^0-9]+/', '', $key);
      if (!in_array($number, $keys)) {
        $keys[] = $number;
      }
    }
    sort($keys);

    return $keys;
  }

  /**
   * Load all webform_submission entities for the current webform that fit to
   * the condition.
   *
   * @param array $element
   * @param string $webform_id
   *
   * @return \Drupal\webform\Entity\WebformSubmission[]
   */
  protected static function getSubmissions(
    array &$element,
    string $webform_id
  ): array {
    /** @var \Drupal\Core\Entity\EntityTypeManager $entityTypeManager */
    $entityTypeManager = Drupal::service('entity_type.manager');
    $webformSubmissionStorage = $entityTypeManager->getStorage(
      'webform_submission'
    );
    $submissionIds = $webformSubmissionStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('webform_id', $webform_id)
      ->execute();

    // Count answers from all qualifying submissions:
    $otherSubmissions = WebformSubmission::loadMultiple($submissionIds);
    $conditionTemplate = $element['#template'];
    if (!empty($conditionTemplate)) {
      foreach ($otherSubmissions as $i => $otherSubmission) {
        if (!WebformTwigExtension::renderTwigTemplate(
          $otherSubmission,
          $conditionTemplate
        )) {
          // Condition exists and was not met.
          unset($otherSubmissions[$i]);
        }
      }
    }

    return $otherSubmissions;
  }

  /**
   * @param array $element
   *   The form element to generate the content for
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   The element
   */
  public static function processWebformComputed(
    array &$element,
    FormStateInterface $form_state
  ): array {
    $element['#tag'] = 'div';
    $element['#value'] = '';

    [$webform_submission, $webform_id] = self::getSubmission(
      $form_state,
      $element
    );
    if (!$webform_submission) {
      return $element;
    }

    // This bar’s data is coming from another webform element.
    $sourceElement = $webform_submission->getWebform()->getElementDecoded(
      $element['#source_element']
    );
    if (!$sourceElement || !($bins = self::getLimitNumbersFromElement(
        $sourceElement
      ))) {
      $element['#value'] = t(
        'Error: Could not find the source element :name!',
        [':name' => $element['#source_element']]
      );

      return $element;
    }
    $element['#attributes']['data-progress-bar-for'] = $element['#source_element'];

    /** @var int[] $counts */
    $counts = [];
    $limitRatios = [];
    foreach ($bins as $number) {
      $counts[$number] = 0;
      $limitRatios[$number] = '';
    }

    // Count answers from all qualifying submissions:
    foreach (self::getSubmissions($element, $webform_id) as $otherSubmission) {
      $choice = $otherSubmission->getElementData($element['#source_element']);
      if ($choice !== NULL && $choice !== '') {
        $number = (int) preg_replace('/[^0-9]+/', '', $choice);
        $counts[$number]++;
      }
    }

    // Prepare output for the 'show_limit_ratios' option:
    foreach (array_filter($counts) as $number => $count) {
      $limitRatios[$number] = '<li>'.t(
          '@count if :number others participate',
          ['@count' => $count, ':number' => $number ?: 'no']
        );
    }

    foreach ($bins as $bin => $number) {
      $binSize = $number == 0 ? 0 : $number - $bins[$bin - 1];
      if ($counts[$number] > $binSize) {
        $overspillToTheRight = $counts[$number] - $binSize;
        if (isset($bins[$bin + 1]) && $counts[$bins[$bin + 1]] + $overspillToTheRight >= $bins[$bin + 1]) {
          if (!empty($element['#show_overspills'])) {
            $limitRatios[$number] .= "<br>".$counts[$bins[$bin + 1]].' + '.$overspillToTheRight.' >= '.$bins[$bin + 1].": ".t(
                'Found overspill of :value to fill limit :number.',
                [':number' => $bins[$bin + 1], ':value' => $overspillToTheRight]
              );
          }

          $counts[$number] = $binSize;
          $counts[$bins[$bin + 1]] += $overspillToTheRight;
        }
      }
    }
    for ($bin = count($bins) - 1; $bin > 0; $bin--) {
      $number = $bins[$bin];
      $binSize = $number == 0 ? 0 : $number - $bins[$bin - 1];
      if ($counts[$number] > $binSize) {
        $overspillToTheLeft = $counts[$number] - $binSize;
        if (!empty($element['#show_overspills'])) {
          $limitRatios[$number] .= "<br>".$counts[$number].' >= '.$binSize.' = '.$number.' - '.$bins[$bin - 1].": ".t(
              'Found overspill of :value to fill limit :number.',
              [':number' => $number, ':value' => $overspillToTheLeft]
            );
        }
        $counts[$number] = $binSize;
        $counts[$bins[$bin - 1]] += $overspillToTheLeft;
      }
    }

    // Calculate which levels were reached:
    /** @var bool[] $levelReached */
    $levelReached = [];
    $committedCount = 0;
    foreach ($bins as $bin => $number) {
      $count = $counts[$number];
      $levelReached[$bin] = $bin == 0 || ($committedCount + $count >= $number && $levelReached[$bin - 1]);
      if ($levelReached[$bin]) {
        $committedCount += $count;
        if (!empty($limitRatios[$number])) {
          $limitRatios[$number] .= '<span class="emoji"> ✅</span>';
        }
      }
    }

    if (!empty($element['#show_limit_ratios'])) {
      $html = join("</li>\n", array_filter($limitRatios)) ? '</li>' : '';
      $element['#value'] .= '<span class="show_limit_ratios">'.t(
          'Count of answers so far'
        ).':</span><ul class="limit-ratios">'.$html.'</ul>';
    }

    self::addBar($element, $bins, $counts, $levelReached, $committedCount);

    $element['#comitted_count'] = $committedCount;
    $element['#bins_end'] = end($bins);

    $element['#value'] = Markup::create($element['#value']);

    return $element;
  }

}
