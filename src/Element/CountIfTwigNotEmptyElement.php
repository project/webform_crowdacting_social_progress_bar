<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Element;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\HtmlTag;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Twig\WebformTwigExtension;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\WebformSubmissionInterface;
use function t;

/**
 * @FormElement("count_if_twig_not_empty")
 */
class CountIfTwigNotEmptyElement extends HtmlTag {

  use CounterElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
        '#process' => [
          [$class, 'processWebformComputed'],
        ],
        '#input' => FALSE,
        '#template' => '',
        '#mode' => NULL,
        '#hide_empty' => FALSE,
        '#debugging_output_per_submission' => FALSE,
        '#webform_submission' => NULL,
        '#theme_wrappers' => ['form_element'],
      ] + parent::getInfo();
  }

  /**
   * {@inheritdoc}
   */
  public static function computeValue(
    array $element,
    WebformSubmissionInterface $webform_submission
  ) {
    // This is called when loading submissions. Return empty string to avoid recursion.
    return '';
  }

  /**
   * @param string $webform_id
   * @param array $element
   */
  public static function fillCounter(string $webform_id, array &$element) {
    $element['#tag'] = 'counter-'.$webform_id.'-'.$element['#admin_title'];

    // Store in cache until a submission is added/deleted/modified or the form was changed using the Build tab.
    $element['#cache']['tags'] = [
      'webform_submission_list',
      'config:webform.webform.'.$webform_id,
    ];
    $element['#cache']['keys'] = [$element['#tag']];

    /** @var \Drupal\Core\Entity\EntityTypeManager $entityTypeManager */
    $entityTypeManager = Drupal::service('entity_type.manager');
    $webformSubmissionStorage = $entityTypeManager->getStorage(
      'webform_submission'
    );
    $submissionIds = $webformSubmissionStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('webform_id', $webform_id)
      ->execute();

    // Load the webform_submission entities.
    $otherSubmissions = WebformSubmission::loadMultiple($submissionIds);
    $counter = 0;
    $debug = '<details>
      <summary>'.t('Debugging output per submission').'</summary>
      <table>';
    foreach ($otherSubmissions as $sid => $otherSubmission) {
      $computed = WebformTwigExtension::renderTwigTemplate(
        $otherSubmission,
        $element['#template']
      );
      if (trim($computed)) {
        $counter++;
      }
      $debug .= '<tr><td>'.$sid.':&nbsp;</td><td>'.$computed.'</td></tr>';
    }
    $debug .= '</table></details>';
    self::addCounter($element, $counter);

    if (!empty($element['#debugging_output_per_submission'])) {
      $element['#value'] .= $debug;
    }
  }

  public static function processWebformComputed(
    array &$element,
    FormStateInterface $form_state
  ) {
    $element['#value'] = '';
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof WebformSubmissionForm) {
      /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
      $webform_submission = $form_object->getEntity();
      $webform_id = $webform_submission->getWebform()->get('id');
    }
    else {
      $element['#tag'] = 'big';
      $element['#value'] = 'ERROR: only submission forms are supported.';

      return $element;
    }
    self::fillCounter($webform_id, $element);

    return $element;
  }

}
