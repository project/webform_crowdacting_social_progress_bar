<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Element;

trait CounterElementTrait {

  /**
   * @param array $element
   * @param int $counter
   */
  protected static function addCounter(array &$element, int $counter): void {
    $template = $element['#counter_template'] ?? '%d';
    $element['#value'] .= '<div class="counter_text">'.str_replace(
        '%d',
        (string) $counter,
        check_markup($template, $element['#counter_template_format'] ?? NULL)
      ).'</div>';
  }

}
