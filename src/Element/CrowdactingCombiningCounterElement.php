<?php

namespace Drupal\webform_crowdacting_social_progress_bar\Element;

use Drupal\Core\Form\FormElementHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * This is a render element displaying a bar or counter.
 * Combines two or more crowdacting social progress bars within a webform.
 *
 * @FormElement("crowdacting_combining_counter")
 */
class CrowdactingCombiningCounterElement extends CrowdactingSocialProgressBarElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
        '#process' => [
          /** @uses processCombineBars */
          [$class, 'processCombineBars'],
        ],
      ] + parent::getInfo();
  }

  /**
   * @param array $element
   *   The form element to generate the content for
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   The element
   */
  public static function processCombineBars(
    array &$element,
    FormStateInterface $form_state,
    &$complete_form
  ) {
    $element['#tag'] = 'div';
    $element['#value'] = '';

    [$webform_submission, $webform_id] = self::getSubmission(
      $form_state,
      $element
    );
    if (!$webform_submission) {
      return $element;
    }

    $sourceElements = array_filter(
      array_map(fn($name) => FormElementHelper::getElementByName(
        $name,
        $complete_form
      ), $element['#source_element'])
    );

    $binsEnd = 0;
    foreach ($sourceElements as $sourceElement) {
      $binsEnd = max($binsEnd, $sourceElement['#bins_end']);
    }
    if (!$binsEnd) {
      $element['#value'] = 'Error: Could not find source elements.';

      return $element;
    }

    // Count answers from all qualifying submissions:
    $counts = [0 => 0, $binsEnd => 0];
    foreach (self::getSubmissions($element, $webform_id) as $otherSubmission) {
      foreach ($sourceElements as $sourceElement) {
        $choice = $otherSubmission->getElementData(
          $sourceElement['#source_element']
        );
        if ($choice !== NULL && $choice !== '') {
          $number = (int) preg_replace('/[^0-9]+/', '', $choice);
          if ($sourceElement['#comitted_count'] >= $number) {
            $counts[0]++;
          }
          break; // Go to next submission
        }
      }
    }

    self::addBar($element, [0, $binsEnd], $counts, [TRUE, FALSE], $counts[0]);
    $element['#value'] = Markup::create($element['#value']);

    return $element;
  }

}
