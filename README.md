# Webform Crowdacting Social Progress Bar

The purpose of this module is to encourage website visitors to participate in a
social action. This can be, for example, a flash mob, boycott, strike, or
public protest.

This module adds a new type of element for Webform module: a progress bar that
counts answers. The bar itself has no input. Instead, you can add any element
from the options family (radios, select). The keys for the options are parsed
for numbers. Each number becomes a subsection in the bar. Please note that all
answer keys that don’t contain a number are considered to be ready even if
no-one else participates. As an example, consider `even_if_nobody_else` key in
this YAML from the Build » Source tab:

````yaml
ready_if:
  '#type': radios
  '#title': 'I will participate in the flash mob'
  '#options':
    even_if_nobody_else: 'no matter how many others take part.'
    100: 'if 100 other people take part.'
    500: 'if 500 other people take part.'
    1000: 'if 1.000 other people take part.'
    5000: 'if 5.000 other people take part.'
bar:
  '#type': crowdacting_social_progress_bar
  '#title': 'Bar 1'
  '#title_display': invisible
  '#source_element': ready_if
  '#show_limit_ratios': true
  '#show_counter': true
  '#counter_template': '%d people are fully committed to participate.'
````

For a much simpler progress bar, see
the [webform_counter](https://drupal.org/project/webform_counter) module. If you
need tokens and count conditions, you can also use the `crowdacting_tokens`
submodule.

## Installation

Install the module as
you [would normally install](https://www.drupal.org/node/1897420) a contributed
Drupal module.

## Add a progress bar to a webform

1. Go to webform's “Build” tab
2. Click on “Add element“. Search for one of the three element types listed
   below
3. Fill all mandatory fields

## Webform element types supplied by this module

- **Submission counter with condition**
  provides an element that shows a counter
  across all submissions of the webform it sits on. Submissions are only
  counted if the Twig condition returns truthy output. Empty strings and 0
  are considered falsy.
- **Crowdacting social progress bar**
  provides an element that shows a progress bar
  and/or a counter. The bar itself has no input, but you can add any element
  from the options family (radios, select). The keys for the options are
  parsed for numbers. Each number becomes a level in the bar. As soon as
  there are enough people in a level, it will be counted and displayed in a
  darker green than unreached levels. You can use a custom theme to change
  colors and styling. Unreached levels fill from the right. If you hover
  over an unreached level, it will show how many answers are still needed to
  reach the level.
- **Crowdacting combining counter**
  combines two or more progress bars into a single progress bar and/or counter.
  If a single submission commits to take part in several actions, it is only
  counted once, as soon as a single level is reached.

## Conditions

You can also add conditions for when to consider answers. For example, the
[webform_double_opt_in](https://www.drupal.org/project/webform_double_opt_in/)
module supplies a key `opt_in_status` that allows users to confirm webform
submissions via email. You could set the progress bar to only count confirmed
submissions. Here is the condition you
want: `{% if 'confirmed' in data.opt_in_status %} 1 {% endif %}`

Also, if you have a negative answer such as “I would never participate“, you
need to add this to the condition, because otherwise it will be counted as if
they answered “I will participate no matter how many others take part.”. If the
key of that answer is `no_participation`, then use the
condition `{% if 'no_participation' not in data.ready_if %} 1 {% endif %}`.

To combine conditions, you can use the operators `and` and `or` within
the `{% if %}`.

For debugging conditions, use the “Submission counter with condition” element
and enable the “Debugging output per submission” option.

Maintainers
-----------

- [Christian “Gogowitsch” Bläul](https://drupal.org/u/gogowitsch)
